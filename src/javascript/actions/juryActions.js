import axios from "axios";
import { calls } from "../utils/calls.js";
import { createError } from "../utils/utils.js";

/**
 * Fetches projects where the user is in the jury
 */
export const getJuriedProject = () => {
	return dispatch => {
		dispatch({ type: 'GET_JURIED_PROJECT' });
		const token = localStorage.getItem('token');
		axios(calls.getJuriedProject(token)).then(res => {
			dispatch({ type: 'GET_JURIED_PROJECT_FULFILLED', payload: { juriedProject: res.data.data } });
		}).catch(err => {
			dispatch({ type: 'GET_JURIED_PROJECT_REJECTED' });
			createError(err, 'Could not get the project you must jury');
		})
	}
};

/**
 * Function to save a deliverable's grade
 * @param data {Object}
 */
export const saveDeliverableGrade = data => {
	return dispatch => {
		dispatch({ type: 'SAVE_DELIVERABLE_GRADE' });
		const token = localStorage.getItem('token');
		axios(calls.saveDeliverableGrade(data, token)).then(() => {
			dispatch({ type: 'SAVE_DELIVERABLE_GRADE_FULFILLED' });
		}).catch(err => {
			dispatch({ type: 'SAVE_DELIVERABLE_GRADE_REJECTED' });
			createError(err, 'Could not save the deliverable grade');
		})
	}
};