import axios from "axios";
import { calls } from "../utils/calls.js";
import { createError } from "../utils/utils.js";

/**
 * Function to get all projects for the professor
 */
export const getProjects = () => {
	return dispatch => {
		dispatch({ type: 'GET_PROJECTS' });
		const token = localStorage.getItem('token');
		axios(calls.getProjects(token)).then(res => {
			dispatch({ type: 'GET_PROJECTS_FULFILLED', payload: { projects: res.data.data } });
		}).catch(err => {
			dispatch({ type: 'GET_PROJECTS_REJECTED' });
			createError(err);
		})
	}
};