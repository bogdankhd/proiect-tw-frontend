import axios from 'axios';
import { calls } from '../utils/calls.js';
import { createError } from "../utils/utils.js";

/**
 * Fetches a project. If empty, user is prompted to create a project
 */
export const getProject = () => {
	return dispatch => {
		dispatch({ type: 'GET_PROJECT' });
		const token = localStorage.getItem('token');
		axios(calls.getProject(token)).then(res => {
			dispatch({ type: 'GET_PROJECT_FULFILLED', payload: { project: res.data.data } });
		}).catch(err => {

			dispatch({ type: 'GET_PROJECT_REJECTED' });
			createError(err, 'Could not get the project');
		})
	}
};

/**
 * Fetches all registered users for the members section on create new project
 */
export const getAllRegisteredUsers = () => {
	return dispatch => {
		dispatch({ type: 'GET_ALL_REGISTERED_USERS' });
		const token = localStorage.getItem('token');
		axios(calls.getAllRegisteredUsers(token)).then(res => {
			dispatch({ type: 'GET_ALL_REGISTERED_USERS_FULFILLED', payload: { allUsers: res.data.data } });
		}).catch(err => {
			dispatch({ type: 'GET_ALL_REGISTERED_USERS_REJECTED' });
			createError(err, 'Could not get all registered users');
		})
	}
};

/**
 * Function to create a project
 * @param data {Object}
 */
export const createProject = data => {
	return dispatch => {
		dispatch({ type: 'CREATE_PROJECT' });
		const token = localStorage.getItem('token');
		axios(calls.createProject(data, token)).then(res => {
			dispatch({ type: 'CREATE_PROJECT_FULFILLED', payload: { savedProject: res.data.data } });
		}).catch(err => {
			dispatch({ type: 'CREATE_PROJECT_REJECTED' });
			createError(err);
		})
	}
};

/**
 * Function to save a deliverable's link
 * @param data {Object}
 */
export const saveDeliverableLink = data => {
	return dispatch => {
		dispatch({ type: 'SAVE_DELIVERABLE_LINK' });
		const token = localStorage.getItem('token');
		axios(calls.saveDeliverableLink(data, token)).then(() => {
			dispatch({ type: 'SAVE_DELIVERABLE_LINK_FULFILLED' });
		}).catch(err => {
			dispatch({ type: 'SAVE_DELIVERABLE_LINK_REJECTED' });
			createError(err, 'Could not save the deliverable link');
		})
	}
};