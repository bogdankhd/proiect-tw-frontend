/**
 * Function to update all inputs in the application
 * @param type {String}
 * @param field {String}
 * @param value {Any}
 * @param otherParams {Object}
 */
export const updateItem = (type, field, value, otherParams) => {
	return dispatch => {
		dispatch({ type: `UPDATE_${ type }`, payload: { field: field, value: value, ...( otherParams || {} ) } })
	}
};

/**
 * Resets an object in a reducer or a reducer
 * @param type {String}
 */
export const resetItem = type => {
	return dispatch => dispatch({ type: `RESET_${ type }` })
};