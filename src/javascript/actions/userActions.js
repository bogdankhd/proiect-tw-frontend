import axios from 'axios';
import { calls } from '../utils/calls.js';
import { createError, deleteToken, saveToken } from "../utils/utils.js";
import { resetItem } from "./generalActions.js";

/**
 * Function to login a user
 * @param data {Object} - from userReducer
 * @param callback {Function} - Send the user where he should be once the login is successful
 */
export const onLogin = (data, callback) => {
	return dispatch => {
		dispatch({ type: 'LOGIN' });
		axios(calls.login(data)).then(res => {
			saveToken(res.data.data.token);
			dispatch({ type: 'LOGIN_FULFILLED', payload: { credentials: res.data.data } });
			if (callback) {
				callback(res.data.data.isAdmin);
			}
		}).catch(err => {
			dispatch({ type: 'LOGIN_REJECTED' });
			createError(err)
		})
	}
};

/**
 * Function to register a user
 * @param data {Object}
 * @param callback {Function} - Send the user where he should be once the register is successful
 */
export const onRegister = (data, callback) => {
	return dispatch => {
		dispatch({ type: 'REGISTER' });
		axios(calls.register(data)).then(res => {
			saveToken(res.data.data.token);
			dispatch({ type: 'REGISTER_FULFILLED', payload: { credentials: res.data.data } });
			if (callback) {
				callback();
			}
		}).catch(err => {
			dispatch({ type: 'REGISTER_REJECTED' });
			createError(err)
		})
	}
};

/**
 * Get credentials of a user
 * @param successCallback
 * @param failureCallback
 */
export const getCredentials = (successCallback, failureCallback) => {
	return dispatch => {
		dispatch({ type: 'GET_CREDENTIALS' });
		const token = localStorage.getItem('token');
		if (!token) {
			dispatch({ type: 'GET_CREDENTIALS_REJECTED' });
			failureCallback();
		} else {
			axios(calls.getCredentials(token)).then(res => {
				dispatch({ type: 'GET_CREDENTIALS_FULFILLED', payload: { credentials: res.data.data } });
				successCallback(res.data.data.isAdmin);
			}).catch(err => {
				dispatch({ type: 'GET_CREDENTIALS_REJECTED' });
				createError(err);
				failureCallback();
			})
		}
	}
};