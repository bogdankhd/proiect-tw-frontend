import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, withRouter } from 'react-router-dom';
import Login from './components/Authentication/Login.jsx';
import Register from './components/Authentication/Register.jsx';
import { ToastContainer } from 'react-toastify';
import { getCredentials } from "./actions/userActions.js";
import AppLoader from "./components/Templates/AppLoader.jsx";
import Project from "./components/Project/Project.jsx";
import ProjectsOverview from "./components/ProjectsOverview/ProjectsOverview.jsx";
import Header from "./components/Templates/Header.jsx";
import { deleteToken } from "./utils/utils.js";

class Index extends PureComponent {
	componentDidMount() {
		const { dispatch } = this.props;
		dispatch(getCredentials(this._successCallback, this._failureCallback))
	}

	_successCallback = isAdmin => {
		const { location, history } = this.props;
		if (isAdmin && (location.pathname !== '/projects-overview')) {
			history.push('/projects-overview');
		} else if (location.pathname === '/') {
			history.push('/project');
		}
	};

	_failureCallback = () => {
		const { history } = this.props;
		history.push('/');
	};

	_onLogout = () => {
		const { history, dispatch } = this.props;
		deleteToken();
		dispatch({ type: 'RESET_USER_REDUCER' });
		dispatch({ type: 'RESET_PROJECTS_OVERVIEW_REDUCER' });
		dispatch({ type: 'RESET_PROJECT_REDUCER' });
		dispatch({ type: 'RESET_JURY_REDUCER' });
		history.push('/');
	};

	render() {
		const { user } = this.props;
		return (
			<>
				{ user.fetchingCredentials ?
					<AppLoader/>
					:
					<>
						{
							user.isLoggedIn ?
								<Header
									onLogout={ this._onLogout }/>
								:
								null
						}
						<Switch>
							<Route path={ `/project` } component={ Project }/>
							<Route path={ `/register` } component={ Register }/>
							<Route path={ `/projects-overview` } component={ ProjectsOverview }/>
							<Route path={ `/` } component={ Login }/>
						</Switch>
						<ToastContainer
							position="top-right"
							autoClose={ 5000 }
							hideProgressBar={ false }
							newestOnTop={ false }
							closeOnClick
							rtl={ false }
							pauseOnVisibilityChange
							draggable
							pauseOnHover/>
					</>
				}

			</>
		)
	}
}

const mapStateToProps = store => ( {
	user: store.user
} );

export default withRouter(connect(mapStateToProps)(Index));
