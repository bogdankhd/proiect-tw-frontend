import { toast } from "react-toastify";

/**
 * Saved the token in localStorage
 * @param token
 */
export const saveToken = token => {
	localStorage.setItem('token', token);
};

/**
 * Gets the token from the localStorage
 */
export const getToken = () => {
	localStorage.getItem('token');
};

/**
 * Deletes the token from localStorage
 */
export const deleteToken = () => {
	localStorage.removeItem('token');
};

export const createError = (err, customMessage = null) => {
	let message = 'An unexpected error has occurred.';
	if (err) {
		if (err.message === 'Network Error') {
			message = 'Could not connect to the Backend Service.'
		}
		if (err.response && err.response.data && err.response.data.error) {
			message = err.response.data.error;
		}
	}

	toast.error('⚠️ ' + ( customMessage || message));

	// In development always log the error
	if (process.env.NODE_ENV === 'development') {
		console.log(err);
	}
};
