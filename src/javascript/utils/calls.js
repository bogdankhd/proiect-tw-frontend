import { BACKEND_URL } from './config.js';

export const calls = {
	login: data => {
		return {
			method: 'POST',
			url: `${ BACKEND_URL }/login`,
			headers: {
				'Content-type': 'application/json'
			},
			data: data
		}
	},
	register: data => {
		return {
			method: 'POST',
			url: `${ BACKEND_URL }/register`,
			headers: {
				'Content-type': 'application/json'
			},
			data: data
		}
	},
	getCredentials: token => {
		return {
			method: 'GET',
			url: `${ BACKEND_URL }/credentials`,
			headers: {
				'Content-type': 'application/json',
				'Authorization': `bearer ${ token }`
			}
		}
	},
	getProject: token => {
		return {
			method: 'GET',
			url: `${ BACKEND_URL }/project`,
			headers: {
				'Content-type': 'application/json',
				'Authorization': `bearer ${ token }`
			}
		}
	},
	getJuriedProject: token => {
		return {
			method: 'GET',
			url: `${ BACKEND_URL }/juried-project`,
			headers: {
				'Content-type': 'application/json',
				'Authorization': `bearer ${ token }`
			}
		}
	},
	getAllRegisteredUsers: token => {
		return {
			method: 'GET',
			url: `${ BACKEND_URL }/all-users`,
			headers: {
				'Content-type': 'application/json',
				'Authorization': `bearer ${ token }`
			}
		}
	},
	createProject: (data, token) => {
		return {
			method: 'POST',
			url: `${ BACKEND_URL }/project`,
			headers: {
				'Content-type': 'application/json',
				'Authorization': `bearer ${ token }`
			},
			data: data
		}
	},
	saveDeliverableLink: (data, token) => {
		return {
			method: 'PUT',
			url: `${ BACKEND_URL }/project/link`,
			headers: {
				'Content-type': 'application/json',
				'Authorization': `bearer ${ token }`
			},
			data: data
		}
	},
	saveDeliverableGrade: (data, token) => {
		return {
			method: 'PUT',
			url: `${ BACKEND_URL }/project/grade`,
			headers: {
				'Content-type': 'application/json',
				'Authorization': `bearer ${ token }`
			},
			data: data
		}
	},
	getProjects: token => {
		return {
			method: 'GET',
			url: `${ BACKEND_URL }/projects`,
			headers: {
				'Content-type': 'application/json',
				'Authorization': `bearer ${ token }`
			}
		}
	}
};