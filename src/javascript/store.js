import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createRootReducer } from './rootReducer.js';
import thunk from 'redux-thunk';

/**
 * Function to generate a new Redux store
 * Also enabled redux-devtools for time-travelling debugging
 */
export default function generateNewStore() {
	return createStore(
		createRootReducer(),
		composeWithDevTools(
			applyMiddleware(thunk),
		),
	);
};
