import { cloneDeep } from "lodash";
import moment from 'moment';

export default function projectsOverviewReducer(state = cloneDeep(defaultState), action) {
	let newState = cloneDeep(state);
	switch (action.type) {
		case 'GET_PROJECTS': {
			newState.fetchingProjects = true;
			return newState;
		}
		case 'GET_PROJECTS_FULFILLED': {
			const { projects } = action.payload;
			newState.projects = projects;
			newState.fetchingProjects = false;
			return newState;
		}
		case 'GET_PROJECTS_REJECTED': {
			newState.fetchingProjects = false;
			return newState;
		}
		case 'RESET_PROJECTS_OVERVIEW_REDUCER': {
			newState = cloneDeep(defaultState);
			return newState;
		}
		default:
			return state;
	}
};

/*
 {
		id: 'asdf',
		title: 'My project',
		description: 'Long description',
		members: [ {
			id: '123',
			fullName: 'Vasilescu'
		}, {
			id: '12312312',
			fullName: 'Asdf'
		} ],
		deliverables: [ {
			id: 'deliverable-1',
			description: 'Deliverable 1 description etc etc',
			endTime: moment('12.12.2019', 'DD.MM.YYYY'),
			link: 'http://timestamp.online/',
			isCompleted: true,
			canBeGraded: false,
			grade: 8,
			allGrades: [7, 8, 9]
		}, {
			id: 'deliverable-2',
			description: 'Deliverable 2 description etc etc',
			endTime: moment('14.12.2019', 'DD.MM.YYYY'),
			link: '',
			isCompleted: false,
			canBeGraded: true,
			grade: 9.5,
			allGrades: [9, 9.5, 10]
		}, {
			id: 'deliverable-3',
			description: 'Deliverable 3 description etc etc',
			endTime: moment('20.12.2019', 'DD.MM.YYYY'),
			link: '',
			isCompleted: false,
			canBeGraded: false,
			grade: 10,
			allGrades: [10, 10, 10]
		}, {
			id: 'deliverable-4',
			description: 'Deliverable 4 description etc etc',
			endTime: moment('24.12.2019', 'DD.MM.YYYY'),
			link: '',
			isCompleted: false,
			canBeGraded: false,
			grade: 4,
			allGrades: [4, 4, 4]
		} ]
	}
 */
const defaultState = {
	projects: [],
	fetchingProjects: false
};