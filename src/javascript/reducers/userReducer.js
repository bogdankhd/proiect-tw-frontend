import { cloneDeep } from "lodash";

export default function registerReducer(state = cloneDeep(defaultState), action) {
	let newState = cloneDeep(state);
	switch (action.type) {
		case 'UPDATE_REGISTER': {
			const { field, value } = action.payload;
			newState.register[field] = value;
			return newState;
		}
		case 'UPDATE_LOGIN': {
			const { field, value } = action.payload;
			newState.login[field] = value;
			return newState;
		}
		case 'LOGIN': {
			newState.loggingIn = true;
			return newState;
		}
		case 'LOGIN_FULFILLED': {
			const { credentials } = action.payload;
			newState.loggingIn = false;
			newState.isLoggedIn = true;
			newState.credentials = credentials;
			delete newState.credentials.token;
			return newState;
		}
		case 'LOGIN_REJECTED': {
			newState.loggingIn = false;
			return newState;
		}
		case 'REGISTER': {
			newState.registering = true;
			return newState;
		}
		case 'REGISTER_FULFILLED': {
			const { credentials } = action.payload;
			newState.registering = false;
			newState.credentials = credentials;
			delete newState.credentials.token;
			newState.isLoggedIn = true;
			return newState;
		}
		case 'REGISTER_REJECTED': {
			newState.registering = false;
			newState.register.confirmPassword = newState.register.password;
			return newState;
		}
		case 'GET_CREDENTIALS': {
			newState.fetchingCredentials = true;
			return newState;
		}
		case 'GET_CREDENTIALS_FULFILLED': {
			const { credentials } = action.payload;
			newState.fetchingCredentials = false;
			newState.isLoggedIn = true;
			newState.credentials = credentials;
			return newState;
		}
		case 'GET_CREDENTIALS_REJECTED': {
			newState.fetchingCredentials = false;
			return newState;
		}
		case 'RESET_REGISTER': {
			newState.register = cloneDeep(defaultRegister);
			return newState;
		}
		case 'RESET_LOGIN': {
			newState.login = cloneDeep(defaultLogin);
			return newState;
		}
		case 'RESET_USER_REDUCER': {
			newState = cloneDeep(defaultState);
			return newState;
		}
		default:
			return state;
	}
};

const defaultRegister = {
	email: '',
	password: '',
	confirmPassword: '',
	fullName: ''
};

const defaultLogin = {
	email: '',
	password: '',
};

const defaultState = {
	register: cloneDeep(defaultRegister),
	login: cloneDeep(defaultLogin),
	loggingIn: false,
	registering: false,
	credentials: {
		id: '',
		fullName: '',
		email: ''
	},
	isLoggedIn: false,
	fetchingCredentials: false
};