import { cloneDeep } from "lodash";
import moment from 'moment';

export default function projectReducer(state = cloneDeep(defaultState), action) {
	let newState = cloneDeep(state);
	switch (action.type) {
		case 'UPDATE_NEW_PROJECT': {
			const { field, value } = action.payload;
			newState.newProject[field] = value;
			return newState;
		}
		case 'UPDATE_PROJECT': {
			const { field, value, index } = action.payload;
			if (field === 'link') {
				newState.project.deliverables[index][field] = value;
			} else {
				newState.project[field] = value;
			}
			return newState;
		}
		case 'GET_ALL_REGISTERED_USERS': {
			newState.fetchingAllUsers = true;
			return newState;
		}
		case 'GET_ALL_REGISTERED_USERS_FULFILLED': {
			const { allUsers } = action.payload;
			newState.fetchingAllUsers = false;
			newState.allUsers = allUsers;
			return newState;
		}
		case 'GET_ALL_REGISTERED_USERS_REJECTED': {
			newState.fetchingAllUsers = false;
			return newState;
		}
		case 'CREATE_PROJECT': {
			newState.creatingProject = true;
			return newState;
		}
		case 'CREATE_PROJECT_FULFILLED': {
			const { savedProject } = action.payload;
			newState.creatingProject = false;
			newState.project = savedProject;
			return newState;
		}
		case 'CREATE_PROJECT_REJECTED': {
			newState.creatingProject = false;
			return newState;
		}
		case 'GET_PROJECT': {
			newState.fetchingProject = true;
			return newState;
		}
		case 'GET_PROJECT_FULFILLED': {
			const { project } = action.payload;
			newState.fetchingProject = false;
			newState.project = project;
			return newState;
		}
		case 'GET_PROJECT_REJECTED': {
			newState.fetchingProject = false;
			return newState;
		}
		case 'SAVE_DELIVERABLE_LINK': {
			newState.savingDeliverableLink = true;
			return newState;
		}
		case 'SAVE_DELIVERABLE_LINK_FULFILLED': {
			newState.savingDeliverableLink = false;
			return newState;
		}
		case 'SAVE_DELIVERABLE_LINK_REJECTED': {
			newState.savingDeliverableLink = false;
			return newState;
		}
		case 'RESET_PROJECT_REDUCER': {
			newState = cloneDeep(defaultState);
			return newState;
		}
		default:
			return state;
	}
};

const defaultProject = {
	title: '',
	description: '',
	members: []
};

const defaultState = {
	newProject: cloneDeep(defaultProject),
	creatingProject: false,
	allUsers: [],
	fetchingAllUsers: false,
	project: null,
	fetchingProject: false,
	savingDeliverableLink: false
};
/*
{
		id: 'asdf',
		title: 'My project',
		description: 'Long description',
		members: [ {
			id: '123',
			fullName: 'Vasilescu'
		}, {
			id: '12312312',
			fullName: 'Asdf'
		} ],
		deliverables: [ {
			id: 'deliverable-1',
			description: 'Deliverable 1 description etc etc',
			endTime: moment('12.12.2019', 'DD.MM.YYYY'),
			link: '',
			isCompleted: true
		}, {
			id: 'deliverable-2',
			description: 'Deliverable 2 description etc etc',
			endTime: moment('14.12.2019', 'DD.MM.YYYY'),
			link: '',
			isCompleted: false
		}, {
			id: 'deliverable-3',
			description: 'Deliverable 3 description etc etc',
			endTime: moment('20.12.2019', 'DD.MM.YYYY'),
			link: '',
			isCompleted: false
		}, {
			id: 'deliverable-4',
			description: 'Deliverable 4 description etc etc',
			endTime: moment('24.12.2019', 'DD.MM.YYYY'),
			link: '',
			isCompleted: false
		} ]
	}
 */