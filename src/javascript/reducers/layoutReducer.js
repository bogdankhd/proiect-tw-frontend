import React from 'react';
import { cloneDeep } from "lodash";
import { Link } from 'react-router-dom';

export default function layoutReducer(state = cloneDeep(defaultState), action) {
	switch (action.type) {
		case '': {

		}
		default:
			return state;
	}
};

const defaultState = {
	authentication: {
		registerTitle: 'Register',
		loginTitle: 'Login',
		registerFooter: () => <>Already have an account? <Link to="/">Login here</Link></>,
		loginFooter: () => <>Don't have an account yet? <Link to="/register">Register now!</Link></>,
		fullName: 'Full Name',
		email: 'Email Address',
		password: 'Password',
		confirmPassword: 'Confirm Password',
		registerButton: 'Register',
		loginButton: 'Login'
	},
	createProject: {
		addProjectTitle: 'Create a new project',
		addProjectFooter: 'Fill in the field above to continue',
		noProjectMessage: 'You are not part of any project yet. Wait for your leader to add you or create one by clicking the button below',
		createProjectButton: 'Create Project',
		title: 'Title',
		description: 'Description ( max. 1000 characters )',
		chooseMembers: 'Choose members'
	},
	juriedProject: {
		notAssigned: 'You have not been assigned to a project yet',
		juriedProjectTitle: 'Project you jury',
		juriedProjectWithTitle: 'You jury: __projectTitle__',
		gradableUntil: 'Can be graded until: __endTime__',
		gradePlaceholder: 'Grades go from 0 to 10'
	},
	project: {
		projectTitle: 'Project __projectName__',
		deliverableUntil: 'Deliverable until __endTime__',
		description: 'Description:',
		members: 'Members:'
	},
	projectsOverview: {
		pageTitle: 'Projects overview',
		tableHeaders: {
			id: 'Id',
			title: 'Title',
			gradeOne: 'Grade 1',
			gradeTwo: 'Grade 2',
			gradeThree: 'Grade 3',
			gradeFour: 'Grade 4',
			actions: 'Actions',
			searchByTitle: 'Search by title...'
		},
		viewModal: {
			title: 'Viewing project "__projectTitle__"',
			titleLabel: 'Title',
			descriptionLabel: 'Description',
			membersLabel: 'Members',
			deliverablesLabel: 'Deliverables',
			deadline: 'Deadline',
			grades: 'Grades ( including first and last given grades )',
			overallGrade: 'Overall Grade'
		}
	},
	generalButtons: {
		saveButton: 'Save',
		closeButton: 'Close',
		logout: 'Logout',
		openButton: 'Open'
	}
};