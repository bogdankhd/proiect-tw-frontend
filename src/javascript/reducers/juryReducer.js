import { cloneDeep } from "lodash";
import moment from 'moment';

export default function juryReducer(state = cloneDeep(defaultState), action) {
	let newState = cloneDeep(state);
	switch (action.type) {
		case 'GET_JURIED_PROJECT': {
			newState.fetchingJuriedProject = true;
			return newState;
		}
		case 'GET_JURIED_PROJECT_FULFILLED': {
			const { juriedProject } = action.payload;
			newState.fetchingJuriedProject = false;
			newState.juriedProject = juriedProject;
			return newState;
		}
		case 'GET_JURIED_PROJECT_REJECTED': {
			newState.fetchingJuriedProject = false;
			return newState;
		}
		case 'SAVE_DELIVERABLE_GRADE': {
			newState.savingDeliverableGrade = true;
			return newState;
		}
		case 'SAVE_DELIVERABLE_GRADE_FULFILLED': {
			newState.savingDeliverableGrade = false;
			return newState;
		}
		case 'SAVE_DELIVERABLE_GRADE_REJECTED': {
			newState.savingDeliverableGrade = false;
			return newState;
		}
		case 'UPDATE_JURIED_PROJECT': {
			const { field, value, index } = action.payload;
			if (field === 'grade') {
				newState.juriedProject.deliverables[index][field] = value;
			} else {
				newState.juriedProject[field] = value;
			}
			return newState;
		}
		case 'RESET_JURY_REDUCER': {
			newState = cloneDeep(defaultState);
			return newState;
		}
		default:
			return state;
	}
};

const defaultState = {
	juriedProject: null,
	fetchingJuriedProject: false
};

/*
{
		id: 'asdf',
		title: 'My project',
		description: 'Long description',
		members: [ {
			id: '123',
			fullName: 'Vasilescu'
		}, {
			id: '12312312',
			fullName: 'Asdf'
		} ],
		deliverables: [ {
			id: 'deliverable-1',
			description: 'Deliverable 1 description etc etc',
			endTime: moment('12.12.2019', 'DD.MM.YYYY'),
			link: 'http://timestamp.online/',
			isCompleted: true,
			canBeGraded: false,
			grade: ''
		}, {
			id: 'deliverable-2',
			description: 'Deliverable 2 description etc etc',
			endTime: moment('14.12.2019', 'DD.MM.YYYY'),
			link: '',
			isCompleted: false,
			canBeGraded: true,
			grade: ''
		}, {
			id: 'deliverable-3',
			description: 'Deliverable 3 description etc etc',
			endTime: moment('20.12.2019', 'DD.MM.YYYY'),
			link: '',
			isCompleted: false,
			canBeGraded: false,
			grade: ''
		}, {
			id: 'deliverable-4',
			description: 'Deliverable 4 description etc etc',
			endTime: moment('24.12.2019', 'DD.MM.YYYY'),
			link: '',
			isCompleted: false,
			canBeGraded: false,
			grade: ''
		} ]
	}
 */