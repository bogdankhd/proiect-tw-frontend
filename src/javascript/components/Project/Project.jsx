import React, { Fragment, PureComponent } from 'react'
import { connect } from 'react-redux';
import { Button, Form, Grid, Header, TextArea, Message, Segment, Dropdown, Card, Input } from 'semantic-ui-react'
import { updateItem } from "../../actions/generalActions.js";
import { createProject, getAllRegisteredUsers, getProject, saveDeliverableLink } from "../../actions/projectActions.js";
import { getJuriedProject, saveDeliverableGrade } from "../../actions/juryActions.js";
import { createError } from "../../utils/utils.js";
import RegularSpinner from "../Templates/RegularSpinner.jsx";
import moment from 'moment';

class Project extends PureComponent {
	state = {
		creatingProject: false
	};

	componentDidMount() {
		const { dispatch } = this.props;
		dispatch(getProject());
		dispatch(getJuriedProject())
	}

	_updateInput = (field, index) => event => {
		const { dispatch } = this.props;
		if (field === 'grade') {
			if (parseInt(event.target.value) < 0 || parseInt(event.target.value) > 10) {
				return;
			}
			dispatch(updateItem('JURIED_PROJECT', field, event.target.value, { index: index }));
		} else if (field === 'link') {
			dispatch(updateItem('PROJECT', field, event.target.value, { index: index }));
		} else {
			dispatch(updateItem('NEW_PROJECT', field, event.target.value));
		}
	};

	_getAllRegisteredUsers = () => {
		const { dispatch } = this.props;
		dispatch(getAllRegisteredUsers());
	};

	_startCreateProject = () => {
		this.setState({ creatingProject: true });
		this._getAllRegisteredUsers();
	};

	_updateMembers = (e, { value }) => {
		const { project, dispatch } = this.props;
		let newMembers = [];
		value.forEach(selectedMemberId => {
			newMembers.push(project.allUsers.find(user => user.id === selectedMemberId));
		});
		dispatch(updateItem('NEW_PROJECT', 'members', newMembers));
	};

	_createProject = () => {
		const { project, dispatch } = this.props;
		if (( project.newProject.members || [] ).length > 4) {
			createError(null, 'You can only add up to 5 members');
			return;
		}
		dispatch(createProject(project.newProject))
	};

	_saveDeliverableLink = deliverable => () => {
		const { project, dispatch } = this.props;
		dispatch(saveDeliverableLink({
			projectId: project.project.id,
			deliverableId: deliverable.id,
			link: deliverable.link
		}))
	};

	_saveDeliverableGrade = deliverable => () => {
		const { jury, dispatch } = this.props;
		dispatch(saveDeliverableGrade({
			projectId: jury.juriedProject.id,
			deliverableId: deliverable.id,
			grade: parseFloat(( parseFloat(deliverable.grade || '') || 0 ).toFixed(2))
		}))
	};

	render() {
		const { creatingProject } = this.state;
		const { user, project, layout, jury } = this.props;

		const usersOptions = project.allUsers.map(user => ( {
			key: user.id,
			text: user.fullName,
			value: user.id
		} )).filter(u => u.key !== user.credentials.userId);

		return (
			<div className="main-page">
				<div className="project-section">
					<Grid textAlign='center' style={ { width: '100%', maxWidth: '100vw' } } verticalAlign='middle'>
						<Grid.Column style={ { maxWidth: '100%' } }>
							<Header
								as='h2'
								color='orange'
								textAlign='center'>
								{
									project.project ?
										layout.project.projectTitle.replace('__projectName__', project.project.title)
										:
										layout.createProject.addProjectTitle
								}
							</Header>
							{
								project.project ?
									<Card fluid>
										<Card.Content>
											<Card.Header>{ project.project.title }</Card.Header>
											<div className="project-description-wrapper">
												<Card.Description>
														<span className="project-description">
															{ layout.project.description }
														</span>
													<br/>
													{ project.project.description }
												</Card.Description>
												<hr/>
												<span className="project-description">
															{ layout.project.members }
														</span>
												{ project.project.members.map((member, i) => (
													<Card.Meta key={ member.id }>
														{ i + 1 }. { member.fullName }
													</Card.Meta>
												)) }
											</div>

										</Card.Content>
										<Card.Content extra>
											{
												[ ...project.project.deliverables.filter(deliverable => deliverable.isCompleted === true), project.project.deliverables.find(deliverable => deliverable.isCompleted === false) ].map((deliverable, i) => {
													return (
														<Fragment key={ i }>
															<Message>{ i + 1 }. { deliverable.description }</Message>
															<div className={ `deliverable` }>
																<Input
																	disabled={ deliverable.isCompleted }
																	placeholder={ `${ i + 1 }. ${ layout.project.deliverableUntil.replace('__endTime__', moment(deliverable.endTime).format('DD.MM.YYYY')) }` }
																	className="deliverable-input"
																	value={ deliverable.link }
																	onChange={ this._updateInput('link', i) }/>
																<Button
																	loading={ project.savingDeliverableLink && !deliverable.isCompleted }
																	disabled={ deliverable.isCompleted }
																	basic
																	color='green'
																	onClick={ this._saveDeliverableLink(deliverable) }>
																	{ layout.generalButtons.saveButton }
																</Button>
															</div>
														</Fragment>
													)
												})
											}
										</Card.Content>
									</Card>
									:
									creatingProject ?
										<>
											<Form
												size='large'
												onSubmit={ this._createProject }
												loading={ project.fetchingAllUsers || project.fetchingProject }>
												<Segment stacked>
													<Form.Input
														fluid={ true }
														value={ project.newProject.title }
														onChange={ this._updateInput('title') }
														placeholder={ layout.createProject.title }
														required/>
													<Form.Input
														value={ project.newProject.description }
														onChange={ this._updateInput('description') }
														control={ TextArea }
														placeholder={ layout.createProject.description }
														required/>
													<Dropdown
														placeholder={ layout.createProject.chooseMembers }
														fluid
														multiple
														search
														selection
														clearable
														options={ usersOptions }
														onChange={ this._updateMembers }/>
													<hr/>
													<Button
														color='orange'
														fluid={ true }
														size='large'
														loading={ project.creatingProject }
														disabled={ project.creatingProject }>
														{ layout.generalButtons.saveButton }
													</Button>
												</Segment>
											</Form>
											<Message>{ layout.createProject.addProjectFooter }</Message>
										</>
										:
										<>
											<Message>{ layout.createProject.noProjectMessage }</Message>
											<Button
												color='orange'
												fluid={ true }
												size='large'
												onClick={ this._startCreateProject }>
												{ layout.createProject.createProjectButton }
											</Button>
										</>
							}

						</Grid.Column>
					</Grid>
				</div>
				<div className="jury-section">
					{ jury.fetchingJuriedProject ?
						<RegularSpinner/>
						:
						jury.juriedProject ?
							<Grid textAlign='center' style={ { width: '100%', maxWidth: '100vw' } }
								  verticalAlign='middle'>
								<Grid.Column style={ { maxWidth: '100%' } }>
									<Header
										as='h2'
										color='orange'
										textAlign='center'>
										{ layout.juriedProject.juriedProjectWithTitle.replace('__projectTitle__', jury.juriedProject.title) }
									</Header>


									<Card fluid>
										<Card.Content>
											<Card.Header>{ jury.juriedProject.title }</Card.Header>
											<div className="project-description-wrapper">
												<Card.Description>
														<span className="project-description">
															{ layout.project.description }
														</span>
													<br/>
													{ jury.juriedProject.description }
												</Card.Description>
												{/*<hr/>
												<span className="project-description">
													{ layout.project.members }
												</span>
												{ jury.juriedProject.members.map((member, i) => (
													<Card.Meta key={ member.id }>
														{ i + 1 }. { member.fullName }
													</Card.Meta>
												)) }*/ }
											</div>

										</Card.Content>
										<Card.Content extra>
											{
												[ ...jury.juriedProject.deliverables.filter(deliverable => deliverable.isCompleted === true), jury.juriedProject.deliverables.find(deliverable => deliverable.isCompleted === false) ].map((deliverable, i) => {
													return (
														<Fragment key={ i }>
															<Message>{ i + 1 }. { deliverable.description }</Message>
															<div className={ `deliverable` }>
																<Input
																	disabled={ true }
																	placeholder={ `${ i + 1 }. ${ layout.juriedProject.gradableUntil.replace('__endTime__', moment(deliverable.endTime).add(5, 'days').format('DD.MM.YYYY')) }` }
																	className="deliverable-input"
																	value={ deliverable.link }
																	onChange={ this._updateInput('link', i) }
																/>
																{ deliverable.link ?
																	<a href={ deliverable.link }
																	   target="_blank"
																	   rel="noopener noreferrer">
																		<Button
																			disabled={ !deliverable.link }
																			basic
																			color='blue'>
																			{ layout.generalButtons.openButton }
																		</Button>
																	</a>
																	:
																	<Button
																		disabled={ !deliverable.link }
																		basic
																		color='blue'>
																		{ layout.generalButtons.openButton }
																	</Button>
																}

															</div>
															<div className={ `deliverable` }>
																<Input
																	type="number"
																	disabled={ !deliverable.canBeGraded }
																	placeholder={ layout.juriedProject.gradePlaceholder }
																	className="deliverable-input"
																	value={ deliverable.grade }
																	onChange={ this._updateInput('grade', i) }
																	min={ 0 }
																	max={ 10 }/>
																<Button
																	loading={ jury.savingDeliverableGrade && deliverable.canBeGraded }
																	disabled={ !deliverable.canBeGraded || parseInt(deliverable.grade) < 0 || parseInt(deliverable.grade) > 10 }
																	basic
																	color='green'
																	onClick={ this._saveDeliverableGrade(deliverable) }>
																	{ layout.generalButtons.saveButton }
																</Button>
															</div>
														</Fragment>
													)
												})
											}
										</Card.Content>
									</Card>

								</Grid.Column>
							</Grid>
							:
							<Grid textAlign='center' style={ { width: '100%', maxWidth: '100vw' } }
								  verticalAlign='middle'>
								<Grid.Column style={ { maxWidth: '100%' } }>
									<Header
										as='h2'
										color='orange'
										textAlign='center'>
										{ layout.juriedProject.juriedProjectTitle }
									</Header>
									<Message>{ layout.juriedProject.notAssigned }</Message>
								</Grid.Column>
							</Grid>
					}
				</div>
			</div>

		)
	}
}

/**
 * Map Redux state to props of React component
 * @param store
 */
const mapStateToProps = store => ( {
	layout: store.layout,
	project: store.project,
	jury: store.jury,
	user: store.user
} );

export default connect(mapStateToProps)(Project);