import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Grid, Header, Button } from "semantic-ui-react";
import ReactTable from 'react-table';
import matchSorter from 'match-sorter';
import { getProjects } from "../../actions/projectsOverviewActions.js";
import ViewModal from "./ViewModal.jsx";

class ProjectsOverview extends PureComponent {
	state = {
		modalOpen: false,
		modalData: {}
	};

	componentDidMount() {
		const { user, history, dispatch } = this.props;
		if (!user.credentials.isAdmin) {
			history.push('/project')
		} else {
			dispatch(getProjects());
		}
	}

	filterMethodInput = (filter, rows, field) => {
		return matchSorter(rows, filter.value, { keys: [ field ] })
	};

	_handleModal = (id, state) => () => {
		const { projectsOverview } = this.props;
		this.setState({
			modalOpen: state,
			modalData: projectsOverview.projects.find(project => project.id === id) || {}
		});
	};

	render() {
		const { modalOpen, modalData } = this.state;
		const { projectsOverview, layout } = this.props;
		const { tableHeaders } = layout.projectsOverview;

		const columns = [ {
			Header: tableHeaders.id,
			accessor: 'id',
			filterable: false,
			width: 250,
			Cell: props => (
				<div className="table-cell">
					{ props.value }
				</div>
			)
		}, {
			Header: tableHeaders.title,
			accessor: 'title',
			filterMethod: (filter, rows) =>
				this.filterMethodInput(filter, rows, 'title'),
			filterAll: true,
			Cell: props => (
				<div className="table-cell">
					{ props.value }
				</div>
			),
			Filter: ({ filter, onChange }) => (
				<input type='text'
					   className='table-filter-input'
					   placeholder={ tableHeaders.searchByTitle }
					   value={ filter ? filter.value : '' }
					   onChange={ event => onChange(event.target.value) }
				/>
			)

		}, {
			Header: tableHeaders.gradeOne,
			accessor: 'deliverables',
			filterable: false,
			width: 100,
			Cell: props => (
				<div className="table-cell">
					{ props.value[0].grade }
				</div>
			)
		}, {
			Header: tableHeaders.gradeTwo,
			accessor: 'deliverables',
			filterable: false,
			width: 100,
			Cell: props => (
				<div className="table-cell">
					{ props.value[1].grade }
				</div>
			)
		}, {
			Header: tableHeaders.gradeThree,
			accessor: 'deliverables',
			filterable: false,
			width: 100,
			Cell: props => (
				<div className="table-cell">
					{ props.value[2].grade }
				</div>
			)
		}, {
			Header: tableHeaders.gradeFour,
			accessor: 'deliverables',
			filterable: false,
			width: 100,
			Cell: props => (
				<div className="table-cell">
					{ props.value[3].grade }
				</div>
			)
		}, {
			Header: tableHeaders.actions,
			accessor: 'id',
			filterable: false,
			width: 100,
			Cell: props => (
				<div className="table-cell">
					<div className="icon-wrapper">
						<Button
							onClick={ this._handleModal(props.value, true) }
							icon='eye'
							color='blue'
							size='tiny'/>
					</div>
				</div>
			)
		} ];

		const defaultSorted = [
			{
				id: 'title',
				asc: true
			}
		];

		return (
			<Grid textAlign='center' style={ { height: '100vh' } } verticalAlign='middle'>
				<Grid.Column style={ { maxWidth: '100vw' } }>
					<Header
						as='h2'
						color='orange'
						textAlign='center'
						className="projects-overview-title">
						{ layout.projectsOverview.pageTitle }
					</Header>
					<div className="projects-overview-table-wrapper">
						<ReactTable
							className='-striped -highlight'
							loading={ projectsOverview.fetchingProjects }
							defaultPageSize={ 15 }
							pageSizeOptions={ [ 15, 40, 80, 160, 320 ] }
							data={ projectsOverview.projects }
							columns={ columns }
							defaultSorted={ defaultSorted }
							filterable
							defaultFilterMethod={ (filter, row) =>
								String(row[filter.id]) === filter.value }
						/>
						<ViewModal
							handleModal={ this._handleModal }
							modalOpen={ modalOpen }
							modalData={ modalData }/>
					</div>
				</Grid.Column>
			</Grid>
		)
	}
}

const mapStateToProps = store => ( {
	layout: store.layout,
	projectsOverview: store.projectsOverview,
	user: store.user
} );

export default connect(mapStateToProps)(ProjectsOverview);