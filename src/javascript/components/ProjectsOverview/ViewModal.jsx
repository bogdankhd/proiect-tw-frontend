import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import { Modal, Button, Grid, Form, Input, Card, Header } from "semantic-ui-react";
import moment from 'moment';

class ViewModal extends PureComponent {
	render() {
		const { layout, modalOpen, modalData, handleModal } = this.props;
		const { viewModal } = layout.projectsOverview;

		return (
			<Modal
				open={ modalOpen }
				closeOnEscape={ false }
				closeOnDimmerClick={ false }>
				<Modal.Header>{ viewModal.title.replace('__projectTitle__', modalData.title) }</Modal.Header>
				<Modal.Content image scrolling>
					<Modal.Description>
						<Grid textAlign='center' style={ { width: '100%', maxWidth: '100vw' } } verticalAlign='middle'>
							<Grid.Column style={ { maxWidth: '100%' } }>
								<Form>
									<Form.Field className="modal-field">
										<label>{ viewModal.titleLabel }</label>
										<Input value={ modalData.title } disabled/>
									</Form.Field>
									<Form.Field className="modal-field">
										<label>{ viewModal.descriptionLabel }</label>
										<Input value={ modalData.description } disabled/>
									</Form.Field>
									<Form.Field className="modal-field">
										<label>{ viewModal.membersLabel }</label>
										<div className="modal-list">
											{ ( modalData.members || [] ).map((member, i) => (
												<p key={ member.id }>
													{ i + 1 }. { member.fullName }
												</p>
											)) }
										</div>
									</Form.Field>
									<Form.Field className="modal-field">
										<label>{ viewModal.deliverablesLabel }</label>
										<div className="modal-deliverables">
											{ ( modalData.deliverables || [] ).map((deliverable, i) => (
												<Card
													fluid
													key={ deliverable.id }>
													<Card.Content>
														<Card.Header>{ deliverable.description }</Card.Header>
														<div className="modal-deliverables-wrapper">
															<Card.Description>
																<span
																	className="modal-deliverables-labels">{ viewModal.deadline }</span>
																<br/>
																<p>{ moment(deliverable.endTime).format('DD.MM.YYYY, HH:mm:ss') }</p>
																<hr/>
																<span
																	className="modal-deliverables-labels">{ viewModal.overallGrade }</span>
																<br/>
																<p>{ deliverable.grade }</p>
																<hr/>
																<span className="modal-deliverables-labels">
																	{ viewModal.grades }
																</span>
																<br/>
																<p>
																	{ ( deliverable.grades || [] ).map((grade, i) => {
																		if (i === deliverable.grades.length - 1) {
																			return (
																				<Fragment key={ i }>{ grade }</Fragment>
																			)
																		}
																		return (
																			<Fragment key={ i }>{ grade }, </Fragment>
																		)
																	}) }
																</p>
																<hr/>
																<div className={ `deliverable` }>
																	<Input
																		disabled={ true }
																		placeholder={ `${ i + 1 }. ${ layout.juriedProject.gradableUntil.replace('__endTime__', moment(deliverable.endTime).add(5, 'days').format('DD.MM.YYYY')) }` }
																		className="deliverable-input"
																		value={ deliverable.link }
																	/>
																	{ deliverable.link ?
																		<a href={ deliverable.link }
																		   target="_blank"
																		   rel="noopener noreferrer">
																			<Button
																				disabled={ !deliverable.link }
																				basic
																				color='blue'
																				onClick={ () => window.open(deliverable.link, '_blank') }>
																				{ layout.generalButtons.openButton }
																			</Button>
																		</a>
																		:
																		<Button
																			disabled={ !deliverable.link }
																			basic
																			color='blue'>
																			{ layout.generalButtons.openButton }
																		</Button>
																	}

																</div>
															</Card.Description>
														</div>
													</Card.Content>
												</Card>
											)) }
										</div>
									</Form.Field>
								</Form>
							</Grid.Column>
						</Grid>
					</Modal.Description>
				</Modal.Content>
				<Modal.Actions>
					<Button icon='close'
							onClick={ handleModal(null, false) }
							content={ layout.generalButtons.closeButton }/>
				</Modal.Actions>
			</Modal>
		)
	}
}

const mapStateToProps = store => ( {
	layout: store.layout
} );

export default connect(mapStateToProps)(ViewModal);