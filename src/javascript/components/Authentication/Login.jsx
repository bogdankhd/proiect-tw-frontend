import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react'
import { resetItem, updateItem } from "../../actions/generalActions.js";
import { onLogin } from "../../actions/userActions.js";

class Login extends PureComponent {
	componentDidMount() {
		const { user } = this.props;
		if (user.isLoggedIn) {
			this._loginSuccessCallback();
		}
	}

	_updateInput = field => event => {
		const { dispatch } = this.props;
		dispatch(updateItem('LOGIN', field, event.target.value));
	};

	_loginSuccessCallback = isAdmin => {
		const { history } = this.props;
		if (isAdmin) {
			history.push('/projects-overview');
		} else {
			history.push('/project');
		}
	};

	_onLogin = () => {
		const { user, dispatch } = this.props;
		dispatch(onLogin(user.login, this._loginSuccessCallback))
	};

	componentWillUnmount() {
		const { dispatch } = this.props;
		dispatch(resetItem('LOGIN'))
	}

	render() {
		const { user, layout } = this.props;
		return (
			<Grid textAlign='center' style={ { height: '100vh' } } verticalAlign='middle'>
				<Grid.Column className='form-custom'>
					<Header as='h2' color='orange' textAlign='center'>{ layout.authentication.loginTitle }</Header>
					<Form size='large'
						  onSubmit={ this._onLogin }>
						<Segment stacked>
							<Form.Input
								value={ user.login.email }
								onChange={ this._updateInput('email') }
								fluid={ true }
								placeholder={ layout.authentication.email }
								type='email'
								name='email'
								icon='mail'
								iconPosition='left'
								required
								autoFocus
							/>
							<Form.Input
								value={ user.login.password }
								onChange={ this._updateInput('password') }
								fluid={ true }
								placeholder={ layout.authentication.password }
								icon='lock'
								iconPosition='left'
								type='password'
								name='password'
								required
								minLength='7'
							/>
							<Button
								color='orange'
								fluid={ true }
								size='large'
								type='submit'
								loading={ user.loggingIn }
								disabled={ user.loggingIn }>
								{ layout.authentication.loginButton }
							</Button>
						</Segment>
					</Form>
					<Message>{ layout.authentication.loginFooter() }</Message>
				</Grid.Column>
			</Grid>
		)
	}
}

const mapStateToProps = store => ( {
	layout: store.layout,
	user: store.user
} );

export default connect(mapStateToProps)(Login);