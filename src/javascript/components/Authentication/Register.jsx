import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react'
import { resetItem, updateItem } from "../../actions/generalActions.js";
import { onRegister } from "../../actions/userActions.js";
import { createError } from "../../utils/utils.js";

class Register extends PureComponent {
	componentDidMount() {
		const { user } = this.props;
		if (user.isLoggedIn) {
			this._registerSuccessCallback();
		}
	}

	_updateInput = field => event => {
		const { dispatch } = this.props;
		dispatch(updateItem('REGISTER', field, event.target.value));
	};

	_registerSuccessCallback = () => {
		const { history } = this.props;
		history.push('/project');
	};

	_onRegister = () => {
		const { user, dispatch } = this.props;
		if (user.register.password !== user.register.confirmPassword) {
			createError(null, 'The two passwords must match');
			return;
		}
		dispatch(onRegister(user.register, this._registerSuccessCallback));
	};

	componentWillUnmount() {
		const { dispatch } = this.props;
		dispatch(resetItem('REGISTER'))
	}

	render() {
		const { user, layout } = this.props;
		return (
			<Grid textAlign='center' style={ { height: '100vh' } } verticalAlign='middle'>
				<Grid.Column className='form-custom'>
					<Header as='h2' color='orange' textAlign='center'>{ layout.authentication.registerTitle }</Header>
					<Form size='large'
						  onSubmit={ this._onRegister }>
						<Segment stacked>
							<Form.Input
								value={ user.register.email }
								onChange={ this._updateInput('email') }
								fluid={ true }
								placeholder={ layout.authentication.email }
								type='email'
								name='email'
								icon='mail'
								iconPosition='left'
								required
								autoFocus
							/>
							<Form.Input
								value={ user.register.password }
								onChange={ this._updateInput('password') }
								fluid={ true }
								placeholder={ layout.authentication.password }
								icon='lock'
								iconPosition='left'
								type='password'
								name='password'
								required
								minLength='7'
							/>
							<Form.Input
								value={ user.register.confirmPassword }
								onChange={ this._updateInput('confirmPassword') }
								fluid={ true }
								placeholder={ layout.authentication.confirmPassword }
								icon='lock'
								iconPosition='left'
								type='password'
								name='password'
								required
								minLength='7'
							/>
							<Form.Input
								value={ user.register.fullName }
								onChange={ this._updateInput('fullName') }
								fluid={ true }
								placeholder={ layout.authentication.fullName }
								icon='user'
								iconPosition='left'
								name='fullname'
								required
							/>

							<Button
								color='orange'
								fluid={ true }
								size='large'
								type='submit'
								loading={ user.registering }
								disabled={ user.registering }>
								{ layout.authentication.registerButton }
							</Button>
						</Segment>
					</Form>
					<Message>{ layout.authentication.registerFooter() }</Message>
				</Grid.Column>
			</Grid>
		)
	}
}

const mapStateToProps = store => ( {
	layout: store.layout,
	user: store.user
} );

export default connect(mapStateToProps)(Register);