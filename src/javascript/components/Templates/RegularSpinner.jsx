import React from 'react';

const RegularSpinner = () => (
	<div className="lds-spinner">
		<div/>
		<div/>
		<div/>
		<div/>
		<div/>
		<div/>
		<div/>
		<div/>
		<div/>
		<div/>
		<div/>
		<div/>
	</div>
);

export default RegularSpinner;