import React from 'react';
import RegularSpinner from "./RegularSpinner.jsx";

const AppLoader = () => {
	return (
		<main className="page-loader-main">
			<RegularSpinner/>
			<p>The page is loading, please wait...</p>
		</main>
	)
};

export default AppLoader;
