import React from 'react';
import { connect } from 'react-redux';
import { Button } from "semantic-ui-react";

const Header = props => (
	<header className="header-custom">
		<Button
			basic
			color='orange'
			onClick={ props.onLogout }>
			{ props.layout.generalButtons.logout }
		</Button>
	</header>
);

const mapStateToProps = store => ( {
	layout: store.layout
} );

export default connect(mapStateToProps)(Header);
