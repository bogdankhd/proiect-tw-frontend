import { combineReducers } from 'redux';
import layoutReducer from './reducers/layoutReducer.js';
import projectReducer from './reducers/projectReducer.js';
import userReducer from './reducers/userReducer.js';
import juryReducer from './reducers/juryReducer.js';
import projectsOverviewReducer from './reducers/projectsOverviewReducer.js';

// Combines all Reducers for Redux
export const createRootReducer = () => combineReducers({
	layout: layoutReducer,
	project: projectReducer,
	user: userReducer,
	jury: juryReducer,
	projectsOverview: projectsOverviewReducer
});
