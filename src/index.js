import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import generateNewStore from './javascript/store.js'
import Index from './javascript/Index.js';
import * as serviceWorker from './serviceWorker';
import moment from 'moment';

import 'react-toastify/dist/ReactToastify.css';
import 'semantic-ui-css/semantic.min.css'
import 'react-table/react-table.css';
import './css/main.css';

moment.locale('ro');

const store = generateNewStore();

ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<Index />
		</BrowserRouter>
	</Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
